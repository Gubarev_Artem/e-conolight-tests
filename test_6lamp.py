# -*- coding: utf-8 -*-
'''
Created on 201-19-05

@author: Dmitry.Dubovitsky
'''
import unittest
import random, string
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait  
from selenium.common.exceptions import TimeoutException  
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

import time
import config
from selenium.selenium import selenium

def random_string(chars = 6):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(chars))

def wait_for_ajax(webdriver):
    def ajax_presented():
        try:
            webdriver.find_element_by_css_selector('#loading-mask2')
        except NoSuchElementException:
            return False
        return True
        
    if ajax_presented():
        WebDriverWait(webdriver, timeout = config.global_timeout).until(lambda el:
                                                                            (el.find_element_by_css_selector('#loading-mask2').is_displayed() == False), message="Ajax is still active")
    return True

class Test(unittest.TestCase):

    def setUp(self):
        if config.default_browser is 'Firefox':
            self.wd = webdriver.Firefox()
            #self.wd.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS) 
        else:
            #default browser is Chrome
            self.wd = webdriver.Chrome(config.chrome_driver_url) 


        self.wd.get(config.main_url)
        
        self.testSuite = unittest.TestSuite()

    def tearDown(self):
        self.wd.close()
        

    def test6lampproduct(self):
        '''
        Test for find special item into catalog & add to cart
        '''
        self.wd.get(config.main_url + config.six_lamp_comm_url)
        while True: #check all options chain
            
            containers = self.wd.find_elements_by_css_selector(config.six_lamp_options_label_parent)
            containers_filtered = [x for x in containers if x.is_displayed() and x.is_enabled()] 
            
            container = containers_filtered[-1]
            
            try:
                input_labels = container.find_elements_by_css_selector(config.six_lamp_options_label)
                input_labels = [x for x in input_labels if x.is_displayed() and x.is_enabled()] 
                
                selected_label = random.choice(input_labels)
                selected_label.click()
                wait_for_ajax(self.wd)
        
                #check something =)
                ###self.assertTrue(True, msg='Can`t select option')
            except NoSuchElementException:
                self.assertTrue(False, msg='Can`t choose an option in coonfigurable product')
            if len(containers) == len(containers_filtered):
                break # All options are selected

        #All options selected
        try:
            wait_for_ajax(self.wd)
            configure_button = self.wd.find_element_by_css_selector(config.configure_button)
            configure_button.click()
            #somesleep
            #check in cart
            wait_for_ajax(self.wd)
            addtocart_button = self.wd.find_element_by_css_selector(config.add_to_cart_selector)
            addtocart_button.click()
            
            #ajax show after page loadind
            time.sleep(5)
            wait_for_ajax(self.wd)#not shore this
            
            self.wd.find_element_by_css_selector(config.cart_top).click()
            wait_for_ajax(self.wd)
            
            cart_items = self.wd.find_elements_by_css_selector(config.cart_items_headers)
            self.assertIn(config.six_lamp_comm_text, [a.text for a in cart_items], msg='Can`t find %s in cart' % config.six_lamp_comm_text)
            
            
        except NoSuchElementException:
            self.assertTrue(False, msg='Can`t find cart or configure locator')
            

if __name__ == "__main__":
    unittest.main()
    