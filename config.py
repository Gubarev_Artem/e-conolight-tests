
main_url = 'http://www.e-conolight.com'
global_timeout = 150


import platform
if platform.system() is 'Windows':
    chrome_driver_url = 'c://chromedriver.exe'
else:
    #Linux VM
    chrome_driver_url = '/home/selenium/selenium/chromedriver'
    


#default_browser = 'Chrome', 'Firefox'
default_browser = 'Firefox'

login = 'dmitry.dubovitsky@alpineinc.com'
password = 'testtesttest'

register_link = "//*[@class='header-account-nav']/a[contains(text(),'New Customer')]"
logout_link = "//*[@class='header-account-nav']/a[contains(text(),'Log Out')]"
login_link = "//*[@class='header-account-nav']/a[contains(text(),'Sign In')]"
customer_page_class = 'customer-account-create'

search_items = 'led lamps', 'fluorescent', 'cord'

#Six Lamp config
six_lamp_comm_text = '6-Lamp Commercial Linear Fluorescent'
six_lamp_comm_url = '/shop-by-product/linear-fluorescent-high-bay/6-lamp-commercial.html'
six_lamp_options_label_parent = '.product-attribute-container'
six_lamp_options_label = '.input-container input' #'.input-container label
configure_button ='.category-configure-product-redirect'
add_to_cart_selector = '#add-to-cart'
cart_top = '#header-cart-icon'
cart_items_headers = '.product-name>a'

