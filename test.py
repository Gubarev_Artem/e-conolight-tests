# -*- coding: utf-8 -*-
'''
Created on 201-19-05

@author: Dmitry.Dubovitsky
'''
import unittest
import random, string
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait  
from selenium.common.exceptions import TimeoutException  
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

import time
import config
from selenium.selenium import selenium
from telnetlib import LOGOUT
from email import email

def random_string(chars = 6):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(chars))


class Test(unittest.TestCase):

    def setUp(self):
         
        if config.default_browser is 'Firefox':
            self.wd = webdriver.Firefox()
        else:
            #default browser is Chrome
            self.wd = webdriver.Chrome(config.chrome_driver_url)

        self.wd.get(config.main_url)
        
        self.testSuite = unittest.TestSuite()

    def tearDown(self):
        self.wd.close()
        
    
    def testLogin(self):
        '''
        test success and fault on login
        '''
        email = config.login
        password = config.password
        
        self.wd.get(config.main_url)
        
        self.assertFalse(self.login(email, random_string()),   msg='Can login with wrong password')
        self.assertTrue( self.login(email, password), msg='Can`t login with provided credentials')
        
    def testLogout(self):
        '''
        test success and fault on logou
        '''
        email = config.login
        password = config.password
        self.wd.get(config.main_url)
        
        self.assertTrue( self.login(email, password), msg='Can`t login duiring testing logout')
        self.assertTrue( self.logout(), msg='Can`t login duiring testing logout')
        

        
    def testSearch(self):
        '''
        test search page
        '''
        self.wd.get(config.main_url)
        for word in config.search_items:
            search_box = self.wd.find_element_by_id('search')
            search_box.clear() #set focus
            search_box.send_keys(word)
            search_box.submit()
            
            try:
                res_images = self.wd.find_elements_by_class_name('product-image')
                self.assertGreater(len(res_images), 1, msg="So small search results. Search for :" + word)
            except NoSuchElementException:
                self.assertTrue(False, msg='Cant find results`s images')
        #Seach for random string
        search_box = self.wd.find_element_by_id('search')
        search_box.clear()
        search_box.send_keys(random_string(10))
        search_box.submit()
        
        try:
            self.wd.find_element_by_class_name('product-image')
            self.assertFalse(True, msg='Expected no data during run this test.')
        except NoSuchElementException:
            #Expected behaivor
            pass
        
    def testRegistation(self):
        '''
        Random name register
        '''
        self.wd.get(config.main_url)
        link = self.wd.find_element_by_xpath(config.register_link)
        link.click()
        page_sign = self.wd.find_elements_by_class_name(config.customer_page_class)
        self.assertIsNotNone(page_sign, msg="Not reigister page open")
        
        self.wd.find_element_by_css_selector('#prefix option').click()
        self.wd.find_element_by_css_selector('#firstname').send_keys('autotest_'+random_string())
        self.wd.find_element_by_css_selector('#lastname').send_keys('autotest_'+random_string())
        
        customer_email = random_string() + '@example.com'
        customer_password = random_string()
        
        self.wd.find_element_by_css_selector('#email_address').send_keys(customer_email)
        
        self.wd.find_element_by_css_selector('#password').send_keys(customer_password)
        self.wd.find_element_by_css_selector('#confirmation').send_keys(customer_password)
        
        self.wd.find_element_by_css_selector(".account-create button[type='submit']").click()
        
        self.assertIsNotNone(self.wd.find_element_by_class_name('customer-account-index'), msg='Unsuccessfull registration')

        self.logout()
        
        self.assertTrue( self.login(customer_email, customer_password), msg='Can login with created credentionals')
        
        
    def login(self, email, password):
        '''
        Logout if accesable logout link, and login with different provided credentional
        '''
        if self.is_loggedin():
            self.logout()
        
        login_href = self.wd.find_element_by_xpath(config.login_link)
        login_href.click()
        
        self.wd.find_element_by_css_selector('#login-form #email').send_keys(email)
        self.wd.find_element_by_css_selector('#login-form #pass').send_keys(password)
        self.wd.find_element_by_css_selector('#login-form #send2').click()
        try:
            self.wd.find_element_by_class_name('customer-account-index')
        except NoSuchElementException:
            return False
        return True
            
        
    def is_loggedin(self):
        try:
            self.wd.find_element_by_xpath(config.logout_link)
        except NoSuchElementException:
            return False
        return True
        
    def logout(self):
        '''
        click logout if nessesary
        '''
        if self.is_loggedin():
            logout_href = self.wd.find_element_by_xpath(config.logout_link)
            logout_href.click()
        try:
            self.wd.find_element_by_xpath(config.login_link) #Check for log-in links
        except NoSuchElementException:
            return False
        return True


if __name__ == "__main__":
    unittest.main()
    